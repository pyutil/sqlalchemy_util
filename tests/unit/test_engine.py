import os
import sqlite3

from pytest import raises

from sqlalchemy_util.engine import connect, create_engine

FILE = os.path.join(os.path.abspath(os.path.dirname(__file__)), "test_db")
AUTH = type(
    "Auth",
    (),
    {
        "value": {
            "database": FILE,
        },
        "refresh": lambda *args: None,
    },
)()
INIT = [
    "CREATE TABLE example ( id int PRIMARY KEY )",
    "INSERT INTO example (id) VALUES (0), (1), (2)",
]


class TestConnect:
    def test_invalid_auth(self):
        with raises(AttributeError):
            connect(None, None, {})

    def test_invalid_dialect(self):
        with raises(AttributeError):
            connect(AUTH, None, {})

    def test_invalid_cparams(self):
        with raises(AttributeError):
            connect(AUTH, sqlite3, None)

    def test_connect(self):
        conn = connect(AUTH, sqlite3, {})
        for query in INIT:
            cursor = conn.cursor()
            cursor.execute(query)
            conn.commit()
            cursor.close()
        assert len(conn.execute("SELECT * from example").fetchall()) == 3
        conn.close()
        os.remove(FILE)
