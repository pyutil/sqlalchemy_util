from pytest import raises
from sqlalchemy import Column, String
from sqlalchemy.exc import UnboundExecutionError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from sqlalchemy_util.model import _ModelBase, model_base


class TestModelFactory:
    DATA_DECL_KWARGS = {
        "class_registry": {},
        "metaclass": type,
    }
    DATA_SESS_KWARGS = {
        "class_": int,
        "autoflush": False,
    }

    def test_basic(self):
        Model = model_base()
        assert _ModelBase in Model.__base__.mro()
        assert Model.metadata.bind is None
        assert Model.session().bind is None
        m = Model(id=1)
        assert m.id == 1
        with raises(AttributeError):
            m.to_json()

    def test_advanced(self):
        Model = model_base(
            name="ModelBase",
            bind=1,
            declarative_kwargs=self.DATA_DECL_KWARGS,
            sessionmaker_kwargs=self.DATA_SESS_KWARGS,
        )
        assert Model.__name__ == "ModelBase"
        assert Model.metadata.bind == 1
        assert type(Model) is type
        assert Model._decl_class_registry is self.DATA_DECL_KWARGS["class_registry"]
        assert int in Model._sessionmaker.class_.mro()
        assert not Model._sessionmaker.kw["autoflush"]
        with raises(TypeError):
            Model.session()


class TestModel:
    DATA_OBJ = {
        "id": 1,
        "name": "aaa",
    }

    def test_derivate(self):
        Model = model_base()

        class C(Model):
            pass

        for attr in (
            "id",
            "session",
            "create_many",
            "create_one",
            "select",
            "delete",
            "to_json",
        ):
            assert hasattr(C, attr)

    def test_instantiation(self):
        Model = model_base()

        class C(Model):
            pass

        c1 = C()
        c2 = C(id=1)
        assert c1.id is None
        assert c2.id == 1

    def test_jsonification(self):
        Model = model_base()

        class C(Model):
            name = Column(String(50))

        c = C(**self.DATA_OBJ)
        assert c.to_json() == self.DATA_OBJ
        assert c.to_json([]) == {}
        assert c.to_json(["id"]) == {"id": self.DATA_OBJ["id"]}

    def test_crud(self):
        Model = model_base()

        class C(Model):
            pass

        with raises(UnboundExecutionError):
            C.create_one(id=1)
        with raises(UnboundExecutionError):
            C.create_many([{"id": i} for i in range(4)])
        with raises(UnboundExecutionError):
            C.select()
        with raises(UnboundExecutionError):
            C.delete()
