from os import environ

from sqlalchemy_util.engine import DefaultSqlAuth, EnvironSqlAuth


class TestDefaultSqlAuth:
    DATA_KWARGS = {
        "database": "test_db",
        "username": "test_user",
        "password": "test_pass",
    }

    def test_value(self):
        auth = DefaultSqlAuth(**self.DATA_KWARGS)
        assert auth.value == self.DATA_KWARGS

    def test_refresh(self):
        auth = DefaultSqlAuth(**self.DATA_KWARGS)
        assert auth.value == self.DATA_KWARGS
        auth.refresh()
        assert auth.value == self.DATA_KWARGS


class TestEnvironSqlAuth:
    DATA_MAPPING = {
        "database": "DBNAME",
        "username": "DBUSER",
        "password": "DBPASS",
    }

    def DATA_VALUES(self, idx: int = 0):
        env = {
            "DBNAME": f"test_db{idx}",
            "DBUSER": f"test_user{idx}",
            "DBPASS": f"test_pass{idx}",
        }
        return env, {key: env[variable] for key, variable in self.DATA_MAPPING.items()}

    def test_value(self):
        env, cmp = self.DATA_VALUES()
        environ.update(env)
        auth = EnvironSqlAuth(**self.DATA_MAPPING)
        assert auth.value == cmp

    def test_refresh(self):
        env, cmp = self.DATA_VALUES(1)
        environ.update(env)
        auth = EnvironSqlAuth(**self.DATA_MAPPING)
        assert auth.value == cmp
        env, cmp = self.DATA_VALUES(2)
        environ.update(env)
        auth.refresh()
        assert auth.value == cmp
