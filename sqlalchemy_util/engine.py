from importlib import import_module
from logging import getLogger
from os import environ

from sqlalchemy import create_engine as _create_engine
from sqlalchemy import event

logger = getLogger(__name__)


class DefaultSqlAuth:
    def __init__(self, **kwargs):
        self.value = kwargs

    def refresh(self):
        pass


class EnvironSqlAuth(DefaultSqlAuth):
    def __init__(self, **kwargs):
        self._mapping = kwargs
        self.refresh()

    def refresh(self):
        self.value = {
            param: environ[variable] for param, variable in self._mapping.items()
        }


def connect(authorizer, dialect, cparams):
    cparams.update(authorizer.value)
    dialect_name = getattr(dialect, "name", str(dialect))
    try:
        return dialect.connect(**cparams)
    except Exception as e:
        logger.warning(
            f"failed to authenticate {dialect_name} database connection, refreshing authorizer"
        )
        authorizer.refresh()
        cparams.update(authorizer.value)
        try:
            return dialect.connect(**cparams)
        except Exception as e:
            logger.error(
                f"{e.__class__.__name__} while connecting to {dialect_name} database: {e}"
            )
            raise


def create_engine(dialect: str, authorizer, *args, **kwargs):
    engine = _create_engine(f"{dialect}://", *args, **kwargs)

    @event.listens_for(engine, "do_connect")
    def engine_connect(dialect, conn_rec, cargs, cparams):
        return connect(authorizer, dialect, cparams)

    return engine


def create_authorizer(
    auth_provider: str,
    auth_params: dict,
):
    source_package, provider_path = auth_provider.split("::")
    source_package = (
        import_module(source_package)
        if source_package
        else type(
            "DictWrapper",
            (dict,),
            {"__getattr__": lambda self, attrname: self[attrname]},
        )(globals())
    )
    provider_class = source_package
    for path_node in provider_path.split("."):
        provider_class = getattr(provider_class, path_node)
    return provider_class(
        *auth_params.pop("args", ()),
        **auth_params,
    )
