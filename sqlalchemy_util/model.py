import typing
from logging import getLogger

from sqlalchemy import Column, Integer, and_, or_
from sqlalchemy.exc import IntegrityError, UnboundExecutionError
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.elements import ClauseElement, UnaryExpression


class _ModelBase:
    id = Column(Integer, primary_key=True)

    @declared_attr
    def logger(cls):
        return getLogger(f"{__name__}.{cls.__name__}")

    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    @classmethod
    def session(cls, **session_kwargs):
        return cls._sessionmaker(**session_kwargs)

    @classmethod
    def insert(
        cls,
        values: typing.Union[
            typing.Dict[str, typing.Any], typing.List[typing.Dict[str, typing.Any]]
        ],
        *,
        _session=None,
    ):
        return cls.create(values, _session=_session)

    @classmethod
    def create(
        cls,
        values: typing.Union[
            typing.Dict[str, typing.Any], typing.List[typing.Dict[str, typing.Any]]
        ],
        *,
        _session=None,
    ):
        session = cls.session() if _session is None else _session
        if isinstance(values, list):
            return cls.create_many(values, _session=session)
        return cls.create_one(**values, _session=session)

    @classmethod
    def create_many(
        cls, value_set: typing.List[typing.Dict[str, typing.Any]], *, _session=None
    ):
        session = cls.session() if _session is None else _session
        cls.logger.info(f"adding {len(value_set)} {cls.__name__} instances...")
        output = [
            cls.create_one(_session=session, **obj_spec) for obj_spec in value_set
        ]
        if _session is None:
            session.close()
        return output

    @classmethod
    def create_one(cls, *, _session=None, **values):
        session = cls.session() if _session is None else _session
        result = {"values": values, "action": "create"}
        try:
            obj = cls(**values)
            session.add(obj)
            session.commit()
            if obj.id is None:
                raise IntegrityError(
                    "value constraint not met",
                    tuple(values.values()),
                    IntegrityError,
                )
            result["object"] = obj
        except UnboundExecutionError:
            raise
        except Exception as e:
            cls.logger.warning(
                f"{e.__class__.__name__} while adding {cls.__name__} instance: {e}"
            )
            result["error"] = e
            session.rollback()
        if _session is None:
            session.close()
        return result

    @classmethod
    def select(
        cls,
        filters: typing.Optional[
            typing.Union[
                typing.Type[ClauseElement],
                typing.Dict[str, typing.Any],
                typing.List[
                    typing.Union[
                        typing.Type[ClauseElement], typing.Dict[str, typing.Any]
                    ]
                ],
            ]
        ] = None,
        *,
        order: typing.Optional[typing.Type[UnaryExpression]] = None,
        page_size: typing.Optional[int] = None,
        page_num: typing.Optional[int] = None,
        _session=None,
    ):
        session = cls.session() if _session is None else _session
        query = session.query(cls)
        if filters is not None:
            if not isinstance(filters, list):
                filters = [filters]
            query = query.filter(
                or_(
                    (
                        filter
                        if isinstance(filter, ClauseElement)
                        else and_(
                            getattr(cls, field) == value
                            for field, value in filter.items()
                        )
                    )
                    for filter in filters
                )
            )
        if order is not None:
            query = query.order_by(order)
        if page_size is not None:
            query = query.limit(page_size).offset(
                0 if page_num is None else page_num * page_size
            )
        output = query.all()
        if _session is None:
            session.close()
        return output

    @classmethod
    def _delete_one(
        cls,
        target,
        *,
        _session=None,
    ):
        session = cls.session() if _session is None else _session
        cls.logger.debug(f"deleting instance @{target.id}...")
        result = {"object": target, "action": "delete"}
        try:
            session.delete(target)
            session.commit()
        except UnboundExecutionError:
            raise
        except Exception as e:
            cls.logger.warning(
                f"{e.__class__.__name__} while deleting {cls.__name__} instance: {e}"
            )
            result["error"] = e
            session.rollback()
        if _session is None:
            session.close()
        return result

    @classmethod
    def delete(
        cls,
        filters: typing.Optional[typing.List[typing.Dict[str, typing.Any]]] = None,
        *,
        target=None,
        _session=None,
    ):
        session = cls.session() if _session is None else _session
        if target is not None:
            return cls._delete_one(target, _session=session)
        results = cls.select(filters, _session=session)
        cls.logger.info(f"deleting {len(results)} {cls.__name__} instances...")
        output = [cls.delete(target=result, _session=session) for result in results]
        if _session is None:
            session.close()
        return output

    @classmethod
    def _update_one(
        cls,
        target,
        values: typing.Dict[str, typing.Any],
        *,
        _session=None,
    ):
        session = cls.session() if _session is None else _session
        cls.logger.debug(f"updating instance @{target.id}")
        result = {
            "object": target,
            "action": "update",
            "old_values": target.to_json(),
            "new_values": values,
        }
        try:
            for key, value in values.items():
                setattr(target, key, value)
            session.commit()
        except UnboundExecutionError:
            raise
        except Exception as e:
            cls.logger.warning(
                f"{e.__class__.__name__} while updating {cls.__name__} instance: {e}"
            )
            result["error"] = e
            session.rollback()
        if _session is None:
            session.close()
        return result

    @classmethod
    def _update_all(
        cls,
        targets,
        values: typing.Dict[str, typing.Any],
        *,
        _session=None,
    ):
        session = cls.session() if _session is None else _session
        output = [
            cls._update_one(target, values, _session=session) for target in targets
        ]
        if _session is None:
            session.close()
        return output

    @classmethod
    def update(
        cls,
        values: typing.Union[
            typing.Dict[str, typing.Any],
            typing.List[typing.Dict[str, typing.Any]],
        ],
        filters: typing.Optional[
            typing.Union[
                typing.Dict[str, typing.Any],
                typing.List[typing.Dict[str, typing.Any]],
            ]
        ] = None,
        *,
        target=None,
        _session=None,
    ):
        session = cls.session() if _session is None else _session

        if isinstance(values, dict):
            _values = values
            values = iter(lambda: _values, None)
        if isinstance(filters, dict):
            filters = [filters]

        if target is not None:
            return cls._update_one(target, next(values), _session=session)

        results = cls.select(filters, _session=session)
        cls.logger.info(f"updating {len(results)} {cls.__name__} instances...")
        if filters is None:
            return cls._update_all(results, next(values), _session=session)

        output = []

        for ident, update_data in zip(filters, values):
            matches = list(
                filter(
                    lambda result: all(
                        getattr(result, field) == value
                        for field, value in ident.items()
                    ),
                    results,
                )
            )

            if not matches:
                e = ValueError("no matching entries")
                cls.logger.warning(
                    f"{e.__class__.__name__} while updating {cls.__name__}: {e}"
                )
                output.append(
                    {
                        "action": "update",
                        "new_values": values,
                        "error": e,
                        "filter": ident,
                    }
                )
                continue

            cls.logger.debug(f"{len(matches)} match filter {ident}")
            output.extend(cls._update_all(matches, update_data, _session=session))

        if _session is None:
            session.close()

        return output

    @classmethod
    def columns(cls):
        table = getattr(cls, "__table__", None)
        if table is None:
            return {}

        column_mapping = {}
        for attr_name, attr in cls.__dict__.items():
            column_name = getattr(attr, "name", None)
            if not column_name:
                continue
            if column_name not in table.columns.keys():
                continue
            column_mapping[column_name] = attr
        return column_mapping

    def to_json(self, fields: typing.Optional[typing.List[str]] = None):
        out = {
            column_name: column.__get__(self, self.__class__)
            for column_name, column in self.columns().items()
        }
        return (
            out
            if fields is None
            else {key: value for key, value in out.items() if key in fields}
        )


def model_base(name="Base", bind=None, declarative_kwargs={}, sessionmaker_kwargs={}):
    class DelegatedBase(_ModelBase):
        _sessionmaker = sessionmaker(bind=bind, **sessionmaker_kwargs)

    return declarative_base(
        name=name, bind=bind, cls=DelegatedBase, **declarative_kwargs
    )
