# SQLAlchemy convenience utilities

## Maintainer: aachn3 <n45t31@protonmail.com>
## Site: <https://gitlab.com/pyutil/sqlalchemy_util>
## Version: 3.0.0

### About

Engine module verrides default SQLAlchemy engine creation, providing means to 
utilising custom authorisation schemas (supporting credentials refreshing); Model
module defines abstraction layer to SQLAlchemy ORM, predefining various queries.

### Table of Contents

- [structure](#project-structure)
- [usage](#usage)
  - [examples](#code-samples)
- [support](#support)
  - [requirements](#prerequisites)
  - [installation](#installation)
- [tests](#testing-package-functionality)
  - [unit](#unit-tests)
  - [integration](#integration-tests)
  

### Project Structure

```
```

### Usage

#### Code samples

#### Testing library functionality

##### Unit tests

Format: PyTest

##### Integration tests

Format: None
